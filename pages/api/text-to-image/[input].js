const { createCanvas} = require('canvas')

export default function handler(req, res) {
    const {input} = req.query
    let width = 1440
    let height = 1440
    const canvas = createCanvas(width, height)
    const ctx = canvas.getContext('2d')
    
    var grd = ctx.createLinearGradient(0, 0, width, height);
    grd.addColorStop(0, "#a68ffb");
    grd.addColorStop(1, "#d9b2e8");
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, width, height);

    var rectX = 50;
    var rectY = 150; 
    var rectWidth = canvas.width - (2 * rectX); 
    var rectHeight = canvas.height - (2 * rectY); 

    var grd = ctx.createLinearGradient(rectX, rectY, rectWidth, rectHeight);
    grd.addColorStop(0, "#2b253f");
    grd.addColorStop(1, "#392f3f");
    ctx.fillStyle = grd;

    ctx.fillStyle = grd
    ctx.beginPath();
    ctx.roundRect(rectX, rectY, rectWidth, rectHeight, 20);
    ctx.stroke();
    ctx.fill();


    // ctx.font = '30px Impact'
    // ctx.fillText(input, 0, 50)
    // var text = ctx.measureText(input)
    // ctx.beginPath()
    // ctx.lineTo(50, 102)
    // ctx.lineTo(50 + text.width, 102)
    const dataURL = canvas.toDataURL();
    const base64Data = dataURL.replace(/^data:image\/png;base64,/, '');
    const binaryData = Buffer.from(base64Data, 'base64');
    res.setHeader('Content-Type', 'image/png');
    res.setHeader('Content-Disposition', 'inline; filename=image.png');
    res.send(binaryData);
}
