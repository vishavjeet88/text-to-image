import { useState } from "react";

export default function Home() {
  let textToImage = (text) => {
    if (typeof window !== "undefined") {
    var canvas = document.createElement("CANVAS");
    var context = canvas.getContext("2d");
    context.fillStyle = "tomato";
    context.font = "bold 18px Arial";
    context.fillText(text, (canvas.width / 2) - 17, (canvas.height / 2) + 8);
    const dataURL = canvas.toDataURL();
    return dataURL
    }
  }
  let [data,setData] = useState("")
  return (
    <>
      <img src={textToImage(data)} />
      <input type="text" value={data} onChange={(e)=>setData(e.target.value)} />
    </>
  )
}
